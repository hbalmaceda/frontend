import { createRouter, createWebHistory } from "vue-router";
import Test from "../views/Test.vue";
import HomeView from "../views/HomeView.vue";
import Register from "../views/Register.vue";
import Login from "../views/Login.vue";
import Adjuntos from "../views/SubirArchivo.vue";
import Chat from "../views/Chat.vue";
import Pedidos from "../views/Pedidos.vue";
import MisPedidos from "../views/MisPedidos.vue";
import Perfil from "../views/Perfil.vue";
import Chats from "../views/Chats.vue";
import Checkout from "../views/Checkout.vue";
import ComprasRealizadas from "../views/ComprasRealizadas.vue";

const routes = [
  {
    path: "/home",
    name: "home",
    component: HomeView,
    props: true,
  },
  {
    path: '/comprasrealizadas',
    name: 'comprasrealizadas',
    component: ComprasRealizadas,
    props: true,
  },
  {
    path: "/checkout",
    name: "checkout",
    component: Checkout,
    props: true,
  },
  {
    path: "/chats",
    name: "chats",
    component: Chats,
    props: true,
  },
  {
    path: "/miperfil",
    name: "miperfil",
    component: Perfil,
    props: true,
  },
  {
    path: "/pedidos",
    name: "pedidos",
    component: Pedidos,
    props: true,
  },
  {
    path: "/mispedidos",
    name: "mispedidos",
    component: MisPedidos,
    props: true,
  },
  {
    path: "/chat",
    name: "chat",
    component: Chat,
    props: true,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/AboutView.vue"),
  },
  {
    path: "/test",
    name: "test",
    component: Test,
  },
  {
    path: "/register",
    name: "register",
    component: Register,
  },
  {
    path: "/",
    name: "login",
    component: Login,
  },
  {
    path: "/subiradjuntos",
    name: "subiradjuntos",
    component: Adjuntos,
  },
];

const router = createRouter({
  routes,
  history: createWebHistory(),
});

export default router;
