import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "./index.css";
import axios from "axios";

/* import './assets/main.css' */

const app = createApp(App).use(router).use(createPinia());

app.mount("#app");
