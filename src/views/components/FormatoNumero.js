export default function formatNumber() {
  //(/\D/g, "") la D significa "no digito"
  const formatoNumero = (num) => {
    return num
      .toString()
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  };

  return {
    formatoNumero,
  };
}