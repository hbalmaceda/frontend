import axios from "axios";
import { ref, watch } from "vue";
import { useRouter } from "vue-router";

export default function useTest() {
  const test = ref([]);
  const router = useRouter();
  const productos = ref([]);
  const publicaciones = ref([]);
  const publicacion = ref('');
  const search_producto = ref('');

  const getTest = async () => {
    let response = await axios.get("http://127.0.0.1:8000/api/testNombres");
    let res = response.data;
    test.value = res.data;
  };

  const getProductos = async () => {
    let response = await axios.get("http://127.0.0.1:8000/api/productos");
    let res = response.data;
    productos.value = res.data;
  };

  const storeTest = async (data) => {
    try {
      await axios.post("http://127.0.0.1:8000/api/testNombres/", data);
      /*  await router.push({ name: "home" }); */
    } catch (error) {
      console.log("errorr", error);
    }
  };

  const register = async (data) => {
    try {
      await axios.post("http://127.0.0.1:8000/api/register/", data);
      console.log("Guardaddooo");
      await router.push({ name: "login" });
    } catch (error) {
      console.log("errorr", error);
    }
  };

  const login = async (data) => {
    try {
      let response = await axios.post("http://127.0.0.1:8000/api/login/", data);
      console.log("Inicio de Sesión");
      console.log(response.data.user);
      localStorage.setItem("user_id", response.data.user);
      await router.push({ name: "home", props: response.data.user });
    } catch (error) {
      console.log("Datos incorrectos");
    }
  };

  const getPublicaciones = async () => {
    let response = await axios.get("http://127.0.0.1:8000/api/publicacion", {
      params: {
        campo: 'id',
        direccion: 'asc',
        search_producto: search_producto.value,
      }
    });
    let res = response.data;
    publicaciones.value = res.data;
  };

  const getPublicacion = async (id_publicacion) => {
    let response = await axios.get(
      `http://127.0.0.1:8000/api/publicacion/${id_publicacion}`
    );
    publicacion.value = response.data.data;
    console.log(publicacion.value);
  };

  watch(search_producto, () => {
    getPublicaciones();
  })

  return {
    search_producto,
    publicacion,
    getPublicacion,
    publicaciones,
    getPublicaciones,
    login,
    productos,
    register,
    getTest,
    test,
    storeTest,
  };
}
